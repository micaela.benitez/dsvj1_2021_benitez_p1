﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShipTP2
{
    public class Ship : MonoBehaviour
    {
        public float speed = 10;

        void Update()
        {
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");

            //Debug.Log("Horizontal: " + hor);
            //Debug.Log("Vertical: " + ver);

            Vector3 direccion = new Vector3(hor, 0, ver);

            transform.position += direccion * speed * Time.deltaTime;
        }

        private void OnTriggerEnter(Collider other)
        {
            other.GetComponent<Renderer>().material.color = new Color(1,1,1,0.5f);
        }

        private void OnTriggerExit(Collider other)
        {
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
        }
    }
}
