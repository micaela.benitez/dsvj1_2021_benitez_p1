﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SunTP2
{
    public class Sun : MonoBehaviour
    {
        private float tiempo;
        private float val;

        void Update()
        {
            tiempo += Time.deltaTime;
            val = Mathf.Sin(tiempo);
            GetComponent<Light>().intensity = 1 + val * 2;
        }
    }
}