﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Meteorites : MonoBehaviour
{
    [Serializable]
    public class MeteoriteData
    {
        public float size;
    }

    public Vector3 size;

    public void InitMeteorites(MeteoriteData md)
    {
        size = Vector3.one * md.size;
    }

    void Update()
    {
        transform.localScale = size;

        if (transform.position.y < -100) transform.position = new Vector3(0,0,0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.transform.localScale = collision.transform.localScale - (Vector3.one * 1f);
        Destroy(gameObject);
    }
}
