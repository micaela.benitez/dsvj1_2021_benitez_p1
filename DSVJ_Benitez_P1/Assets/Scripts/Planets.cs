﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetsTP2
{
    public class Planets : MonoBehaviour
    {
        [Serializable]
        public class PlanetData
        {
            public string planetName;

            public float traslationRadius;   // Radio - distancia del objeto del que va a girar
            public float traslationSpeed;   // Velocidad con la que va a girar

            public Vector3 rotationAxis;   // Rotacion sobre si mismo
            public float rotationSpeed;   // Velocidad de la rotacion sobre si mismo

            public float size;

            public Material mat;
        }

        public float speed = 0;
        public float radius = 0;
        public float angle = 0;

        public float rotationAngle = 0;
        public Vector3 wantedScale;
        public float rotationSpeed = 5;
        public Vector3 rotationDirection;

        public void Init(PlanetData pd)
        {
            radius = pd.traslationRadius;
            speed = pd.traslationSpeed;
            rotationDirection = pd.rotationAxis;
            rotationSpeed = pd.rotationSpeed;
            wantedScale = Vector3.one * pd.size;   // Como wantedScale es un Vector3 y size es un float, escalo size
            GetComponent<MeshRenderer>().material = pd.mat;
        }

        private void Start()
        {
            if (name == "Earth")
            {
                transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        void Update()
        {
            Vector3 v3 = Vector3.zero;   // Igualo a (0,0,0) porque esa es la posicion del sol

            angle += speed * Time.deltaTime;

            v3.x = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            v3.z = radius * Mathf.Sin(angle * Mathf.Deg2Rad);
            // Si la pos inicial de X y Z no fuera 0, habria que sumarsela, pero como en este caso es 0 no hace falta

            transform.position = v3;

            transform.localScale = wantedScale;
            //transform.rotation = Quaternion.Euler(0, rotationAngle, 0);
            transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
        }
    }
}