﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetTP21
{
    public class Planet : MonoBehaviour
    {
        public float speed = 0;
        public float radius = 0;
        public float angle = 0;

        public float rotationAngle = 0;
        public Vector3 wantedScale;
        public float rotationSpeed = 5;
        public Vector3 rotationDirection;

        void Start()
        {
            speed = 30.0f;
            radius = transform.position.x;
        }

        void Update()
        {
            Vector3 v3 = Vector3.zero;   // Igualo a (0,0,0) porque esa es la posicion del sol

            angle += speed * Time.deltaTime;

            v3.x = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            v3.z = radius * Mathf.Sin(angle * Mathf.Deg2Rad);
            // Si la pos inicial de X y Z no fuera 0, habria que sumarsela, pero como en este caso es 0 no hace falta

            transform.position = v3;

            //transform.localScale = wantedScale;
            //transform.rotation = Quaternion.Euler(0, rotationAngle, 0);
            transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
        }
    }
}




/*
transform.position += Vector3.right * speed * Time.deltaTime;   // Vector 3.right es una constante de vector 3 ya predefinida en (1,0,0)
                                                                // Entonces esto significa que el objeto se va a mover a la derecha a esa velocidad por la cantidad de tiempo que tarden los frames
*/