﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PlanetsTP2;

namespace InstancingTP2
{
    public class Instancing : MonoBehaviour
    {
        public Planets planetPrefab;   // Planets.cpp

        public List<Planets.PlanetData> planetsData;

        public List<Planets> generatedPlanets = new List<Planets>();

        public GameObject moveCamera;

        void Start()
        {
            for (int i = 0; i < planetsData.Count; i++)
            {
                Planets.PlanetData planetData = planetsData[i];

                //GameObject planetsInstantiate = Instantiate(planetPrefab, new Vector3(i * 2, 0, 0), Quaternion.identity).gameObject;   // Instancio los objetos
                GameObject planetsInstantiate = Instantiate(planetPrefab).gameObject;                

                Planets p = planetsInstantiate.GetComponent<Planets>();
                generatedPlanets.Add(p);   // Agrego los objetos instanciados a una lista

                p.Init(planetData);   // Actualizo los datos de cada uno
                planetsInstantiate.name = planetData.planetName;
                
                planetsInstantiate.transform.parent = gameObject.transform;   // Todos los planetas son hijos de instancing                
            }

            //Destroy(gameObject, 1);   // Despues de 1seg va a destruir el instanciador
            //Destroy(this, 1);   // Despues de 1seg va a destruit el script intancing de instanciador
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                moveCamera.SetActive(true);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Sun").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Mercury").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Venus").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Earth").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Mars").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Jupiter").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Saturn").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Uranus").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                GameObject[] planet = GameObject.FindGameObjectsWithTag("Planet");

                foreach (GameObject cam in planet)
                {
                    cam.transform.GetChild(0).gameObject.SetActive(false);
                }

                GameObject.Find("Neptune").transform.GetChild(0).gameObject.SetActive(true);
                moveCamera.SetActive(false);
            }
        }
    }
}
