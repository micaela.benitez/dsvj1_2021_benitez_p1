﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InstancingMeteorites : MonoBehaviour
{
    public Meteorites meteoritePrefab;

    public List<Meteorites.MeteoriteData> meteorites;

    public List<Meteorites> generatedMeteorites = new List<Meteorites>();

    void Update()
    {
        Debug.Log("Time: " + Time.frameCount);

        if (Time.frameCount % 1000 == 0)
        {
            for (int i = 0; i < meteorites.Count; i++)
            {
                Meteorites.MeteoriteData meteoriteData = meteorites[i];

                Vector3 newVec = new Vector3(UnityEngine.Random.Range(20, -20), 20, UnityEngine.Random.Range(30, -30));
                GameObject meteoritesInstantiate = Instantiate(meteoritePrefab, newVec, Quaternion.identity).gameObject;

                meteoritesInstantiate.transform.parent = gameObject.transform;

                Meteorites m = meteoritesInstantiate.GetComponent<Meteorites>();
                generatedMeteorites.Add(m);

                m.InitMeteorites(meteoriteData);
            }
        }
    }
}
